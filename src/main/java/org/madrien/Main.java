package org.madrien;

import org.madrien.dao.implementations.AlbumDAOImpl;
import org.madrien.dao.implementations.OrderDAOImpl;
import org.madrien.dao.implementations.SongDAOImpl;
import org.madrien.entities.Album;
import org.madrien.entities.Order;
import org.madrien.entities.Song;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Persistence;
import java.util.List;
import java.util.Map;

public class Main {
    private static TableData tableData = new TableData();
    private static EntityManagerFactory emf = Persistence.createEntityManagerFactory("homeworkPU");
    private static EntityManager em = emf.createEntityManager();

    public static void main(String[] args) {
        fillTables();

        testSongDAO();
        testOrderDAO();
        testAlbumDAO();

        em.close();
    }

    public static <T> void saveMap(Map<String, T> map, EntityManager em) {
        for (String key : map.keySet()) {
            em.persist(map.get(key));
        }
    }

    public static void fillTables() {
        EntityTransaction transaction = em.getTransaction();
        transaction.begin();
        Map<String, ?> map = tableData.getClients();
        saveMap(map, em);

        map = tableData.getAlbums();
        saveMap(map, em);

        map = tableData.getBands();
        saveMap(map, em);

        map = tableData.getSongs();
        saveMap(map, em);

        map = tableData.getOrders();
        saveMap(map, em);

        transaction.commit();
    }

    public static void testSongDAO() {
        SongDAOImpl songDAO = new SongDAOImpl();
        songDAO.setEntityManager(em);
        List<Song> resultSongs = null;

        resultSongs = songDAO.findSongsByBandNameNative();
        System.out.println("SongDAO native method:");
        for (Song song : resultSongs) {
            System.out.println(song);
        }
        System.out.println();

        resultSongs = songDAO.findSongsByBandNameNamed("The Fall of Troy");
        System.out.println("SongDAO named method:");
        for (Song song : resultSongs) {
            System.out.println(song);
        }
        System.out.println();

        resultSongs = songDAO.findSongsByBandNameJPQL("Gorillaz");
        System.out.println("SongDAO JPQL method:");
        for (Song song : resultSongs) {
            System.out.println(song);
        }
        System.out.println();

        resultSongs = songDAO.findSongsByBandNameCriteria("Puscifer");
        System.out.println("SongDAO criteria method:");
        for (Song song : resultSongs) {
            System.out.println(song);
        }
        System.out.println();
    }

    public static void testOrderDAO() {
        OrderDAOImpl orderDAO = new OrderDAOImpl();
        orderDAO.setEntityManager(em);
        List<Order> resultOrders = null;

        resultOrders = orderDAO.findOrdersWithAlbumNameNative();
        System.out.println("OrderDAO native method:");
        for (Order order : resultOrders) {
            System.out.println(order);
        }
        System.out.println();

        resultOrders = orderDAO.findOrdersWithAlbumNameJPQL("Money Shot");
        System.out.println("OrderDAO JPQL method:");
        for (Order order : resultOrders) {
            System.out.println(order);
        }
        System.out.println();

        resultOrders = orderDAO.findOrdersWithAlbumNameNamed("Demon Days");
        System.out.println("OrderDAO named method:");
        for (Order order : resultOrders) {
            System.out.println(order);
        }
        System.out.println();

        resultOrders = orderDAO.findOrdersWithAlbumNameCriteria("Art of Doubt");
        System.out.println("OrderDAO criteria method:");
        for (Order order : resultOrders) {
            System.out.println(order);
        }
        System.out.println();
    }

    public static void testAlbumDAO() {
        AlbumDAOImpl albumDAO = new AlbumDAOImpl();
        albumDAO.setEntityManager(em);
        List<Album> resultAlbums = null;

        resultAlbums = albumDAO.findAlbumsByDurationNative();
        System.out.println("AlbumDAO native method:");
        for (Album album : resultAlbums) {
            System.out.println(album);
        }
        System.out.println();

        resultAlbums = albumDAO.findAlbumsByDurationJPQL(700L);
        System.out.println("AlbumDAO JPQL method:");
        for (Album album : resultAlbums) {
            System.out.println(album);
        }
        System.out.println();

        resultAlbums = albumDAO.findAlbumsByDurationNamed(700L);
        System.out.println("AlbumDAO named method:");
        for (Album album : resultAlbums) {
            System.out.println(album);
        }
        System.out.println();

        resultAlbums = albumDAO.findAlbumsByDurationCriteria(800L);
        System.out.println("AlbumDAO criteria method:");
        for (Album album : resultAlbums) {
            System.out.println(album);
        }
        System.out.println();
    }
}
