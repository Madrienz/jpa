package org.madrien.entities;

import javax.persistence.*;

@Entity
@Table(name = "songs")
@NamedQuery(name = Song.FIND_SONGS_BY_BAND_NAME, query = "SELECT s "
                                                        + "FROM Song s "
                                                        + "WHERE s.band.name = :name ")
public class Song {

    public static final String FIND_SONGS_BY_BAND_NAME = "findSongsByBandName";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Album album;

    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn
    private Band band;

    @Column
    private int duration;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Album getAlbum() {
        return album;
    }

    public void setAlbum(Album album) {
        this.album = album;
    }

    public Band getBand() {
        return band;
    }

    public void setBand(Band band) {
        this.band = band;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    @Override
    public String toString() {
        return "Song{" +
                "name='" + name + '\'' +
                '}';
    }
}
