package org.madrien.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "albums")
@NamedQuery(name = Album.FIND_ALBUMS_BY_DURATION, query = "SELECT a "
                                                        + "FROM Album a "
                                                        + "JOIN a.songs s "
                                                        + "GROUP BY a "
                                                        + "HAVING SUM(s.duration) < ?1 "
                                                        + "ORDER BY a.name ")
public class Album {

    public static final String FIND_ALBUMS_BY_DURATION = "findAlbumsByDuration";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column
    private String name;

    @OneToMany(mappedBy = "album")
    private List<Song> songs = new ArrayList<>();

    @ManyToMany
    @JoinTable(
            name = "order_album",
            joinColumns = @JoinColumn(name = "album_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id")
    )
    private List<Order> orders = new ArrayList<>();

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Song> getSongs() {
        return songs;
    }

    public void setSongs(List<Song> songs) {
        this.songs = songs;
    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }

    @Override
    public String toString() {
        return "Album{" +
                "name='" + name + '\'' +
                '}';
    }
}
