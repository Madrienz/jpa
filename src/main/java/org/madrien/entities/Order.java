package org.madrien.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "orders")
@NamedQuery(name = Order.FIND_ORDERS_BY_ALBUM_NAME, query = "SELECT o "
                                                            + "FROM Order o "
                                                            + "JOIN o.albums album "
                                                            + "WHERE album.name = :albumName ")
public class Order {

    public static final String FIND_ORDERS_BY_ALBUM_NAME = "findOrdersByAlbumName";

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToMany
    @JoinTable(
            name = "order_album",
            joinColumns = @JoinColumn(name = "order_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "album_id", referencedColumnName = "id")
    )
    private List<Album> albums = new ArrayList<>();

    @OneToOne
    @JoinColumn(name = "client_info")
    private Client client;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Album> getAlbums() {
        return albums;
    }

    public void setAlbums(List<Album> albums) {
        this.albums = albums;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public String toString() {
        return "Order{" +
                "client=" + client.getLastName() +
                '}';
    }
}
