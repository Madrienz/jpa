package org.madrien;

import org.madrien.entities.*;

import java.util.HashMap;
import java.util.Map;

public class TableData {
    Map<String, Client> clients = new HashMap<>();
    Map<String, Order> orders = new HashMap<>();
    Map<String, Album> albums = new HashMap<>();
    Map<String, Song> songs = new HashMap<>();
    Map<String, Band> bands = new HashMap<>();

    public TableData() {
        createClients();
        createBands();
        createAlbums();
        createSongs();
        createOrders();
    }

    public void createClients() {
        Client client = new Client();
        client.setFirstName("John");
        client.setLastName("Masters");
        clients.put(client.getLastName(), client);

        client = new Client();
        client.setFirstName("Bob");
        client.setLastName("Thornton");
        clients.put(client.getLastName(), client);

        client = new Client();
        client.setFirstName("Casey");
        client.setLastName("Affleck");
        clients.put(client.getLastName(), client);

        client = new Client();
        client.setFirstName("Keanu");
        client.setLastName("Reeves");
        clients.put(client.getLastName(), client);

        client = new Client();
        client.setFirstName("Jack");
        client.setLastName("Nicholson");
        clients.put(client.getLastName(), client);
    }

    public void createBands() {
        Band band = new Band();
        band.setName("Puscifer");
        bands.put(band.getName(), band);

        band = new Band();
        band.setName("Arctic Monkeys");
        bands.put(band.getName(), band);

        band = new Band();
        band.setName("The Fall of Troy");
        bands.put(band.getName(), band);

        band = new Band();
        band.setName("Gorillaz");
        bands.put(band.getName(), band);

        band = new Band();
        band.setName("Metric");
        bands.put(band.getName(), band);
    }

    public void createAlbums() {
        Album album = new Album();
        album.setName("Humbug");
        albums.put(album.getName(), album);

        album = new Album();
        album.setName("Art of Doubt");
        albums.put(album.getName(), album);

        album = new Album();
        album.setName("Demon Days");
        albums.put(album.getName(), album);

        album = new Album();
        album.setName("Manipulator");
        albums.put(album.getName(), album);

        album = new Album();
        album.setName("Money Shot");
        albums.put(album.getName(), album);
    }

    /** add three songs per album */
    public void createSongs() {
        Song song = new Song();
        song.setName("The Arsonist");
        song.setAlbum(albums.get("Money Shot"));
        song.setBand(bands.get("Puscifer"));
        song.setDuration(152);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Galileo");
        song.setAlbum(albums.get("Money Shot"));
        song.setBand(bands.get("Puscifer"));
        song.setDuration(317);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("The Remedy");
        song.setAlbum(albums.get("Money Shot"));
        song.setBand(bands.get("Puscifer"));
        song.setDuration(367);
        songs.put(song.getName(), song);


        song = new Song();
        song.setName("Fire And The Thud");
        song.setAlbum(albums.get("Humbug"));
        song.setBand(bands.get("Arctic Monkeys"));
        song.setDuration(237);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("My Propeller");
        song.setAlbum(albums.get("Humbug"));
        song.setBand(bands.get("Arctic Monkeys"));
        song.setDuration(205);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Secret Door");
        song.setAlbum(albums.get("Humbug"));
        song.setBand(bands.get("Arctic Monkeys"));
        song.setDuration(223);
        songs.put(song.getName(), song);


        song = new Song();
        song.setName("Sledgehammer");
        song.setAlbum(albums.get("Manipulator"));
        song.setBand(bands.get("The Fall of Troy"));
        song.setDuration(361);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("The Dark Trail");
        song.setAlbum(albums.get("Manipulator"));
        song.setBand(bands.get("The Fall of Troy"));
        song.setDuration(270);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Semi-Fiction");
        song.setAlbum(albums.get("Manipulator"));
        song.setBand(bands.get("The Fall of Troy"));
        song.setDuration(266);
        songs.put(song.getName(), song);


        song = new Song();
        song.setName("Dark Saturday");
        song.setAlbum(albums.get("Art of Doubt"));
        song.setBand(bands.get("Metric"));
        song.setDuration(230);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Risk");
        song.setAlbum(albums.get("Art of Doubt"));
        song.setBand(bands.get("Metric"));
        song.setDuration(324);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Die Happy");
        song.setAlbum(albums.get("Art of Doubt"));
        song.setBand(bands.get("Metric"));
        song.setDuration(245);
        songs.put(song.getName(), song);


        song = new Song();
        song.setName("Feel Good Inc");
        song.setAlbum(albums.get("Demon Days"));
        song.setBand(bands.get("Gorillaz"));
        song.setDuration(222);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("Kids With Guns");
        song.setAlbum(albums.get("Demon Days"));
        song.setBand(bands.get("Gorillaz"));
        song.setDuration(225);
        songs.put(song.getName(), song);

        song = new Song();
        song.setName("DARE");
        song.setAlbum(albums.get("Demon Days"));
        song.setBand(bands.get("Gorillaz"));
        song.setDuration(245);
        songs.put(song.getName(), song);
    }

    public void createOrders() {
        Order order = new Order();
        order.setClient(clients.get("Affleck"));
        order.getAlbums().add(albums.get("Manipulator"));
        order.getAlbums().add(albums.get("Demon Days"));
        orders.put(order.getClient().getLastName(), order);

        order = new Order();
        order.setClient(clients.get("Masters"));
        order.getAlbums().add(albums.get("Manipulator"));
        order.getAlbums().add(albums.get("Art of Doubt"));
        orders.put(order.getClient().getLastName(), order);

        order = new Order();
        order.setClient(clients.get("Thornton"));
        order.getAlbums().add(albums.get("Humbug"));
        order.getAlbums().add(albums.get("Money Shot"));
        orders.put(order.getClient().getLastName(), order);

        order = new Order();
        order.setClient(clients.get("Reeves"));
        order.getAlbums().add(albums.get("Art of Doubt"));
        order.getAlbums().add(albums.get("Demon Days"));
        orders.put(order.getClient().getLastName(), order);

        order = new Order();
        order.setClient(clients.get("Nicholson"));
        order.getAlbums().add(albums.get("Manipulator"));
        order.getAlbums().add(albums.get("Money Shot"));
        orders.put(order.getClient().getLastName(), order);
    }

    public Map<String, Client> getClients() {
        return clients;
    }

    public Map<String, Order> getOrders() {
        return orders;
    }

    public Map<String, Album> getAlbums() {
        return albums;
    }

    public Map<String, Song> getSongs() {
        return songs;
    }

    public Map<String, Band> getBands() {
        return bands;
    }
}
