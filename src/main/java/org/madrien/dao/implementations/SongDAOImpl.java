package org.madrien.dao.implementations;

import org.madrien.dao.interfaces.SongDAO;
import org.madrien.entities.Song;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

public class SongDAOImpl extends DAOImpl<Song> implements SongDAO {

    public static final String NATIVE_QUERY = "SELECT * FROM songs "
                                            + "LEFT JOIN bands "
                                            + "ON songs.band_id = bands.id "
                                            + "WHERE bands.name = 'Metric'; ";

    public static final String JPQL_QUERY = "SELECT s "
                                        + "FROM Song s "
                                        + "WHERE s.band.name = :name ";

    public SongDAOImpl() {
        super(Song.class);
    }

    @Override
    public List<Song> findSongsByBandNameNative() {
        return em.createNativeQuery(NATIVE_QUERY, Song.class).getResultList();
    }

    @Override
    public List<Song> findSongsByBandNameJPQL(String bandName) {
        return em.createQuery(JPQL_QUERY, Song.class)
                .setParameter("name", bandName)
                .getResultList();
    }

    @Override
    public List<Song> findSongsByBandNameNamed(String bandName) {
        return em.createNamedQuery(Song.FIND_SONGS_BY_BAND_NAME, Song.class)
                .setParameter("name", bandName)
                .getResultList();
    }

    @Override
    public List<Song> findSongsByBandNameCriteria(String bandName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Song> criteriaQuery = cb.createQuery(Song.class);
        Root<Song> song = criteriaQuery.from(Song.class);
        criteriaQuery.select(song)
                .where(cb.equal(song.get("band").get("name"), bandName));
        return em.createQuery(criteriaQuery).getResultList();
    }
}
