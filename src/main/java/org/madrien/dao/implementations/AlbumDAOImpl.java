package org.madrien.dao.implementations;

import org.madrien.dao.interfaces.AlbumDAO;
import org.madrien.entities.Album;
import org.madrien.entities.Song;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class AlbumDAOImpl extends DAOImpl<Album> implements AlbumDAO {

    public static final String NATIVE_QUERY = "SELECT albums.id, albums.name, SUM(songs.duration) FROM songs "
                                            + "INNER JOIN albums "
                                            + "ON songs.album_id = albums.id "
                                            + "INNER JOIN bands "
                                            + "ON songs.band_id = bands.id "
                                            + "GROUP BY albums.id, albums.name "
                                            + "HAVING SUM(songs.duration) < 800 "
                                            + "ORDER BY albums.name; ";

    public static final String JPQL_QUERY = "SELECT a "
                                            + "FROM Album a "
                                            + "JOIN a.songs s "
                                            + "GROUP BY a "
                                            + "HAVING SUM(s.duration) < ?1 "
                                            + "ORDER BY a.name ";

    public AlbumDAOImpl() {
        super(Album.class);
    }

    @Override
    public List<Album> findAlbumsByDurationNative() {
        return em.createNativeQuery(NATIVE_QUERY, Album.class).getResultList();
    }

    @Override
    public List<Album> findAlbumsByDurationJPQL(long duration) {
        return em.createQuery(JPQL_QUERY, Album.class)
                .setParameter(1, duration)
                .getResultList();
    }

    @Override
    public List<Album> findAlbumsByDurationNamed(long duration) {
        return em.createNamedQuery(Album.FIND_ALBUMS_BY_DURATION, Album.class)
                .setParameter(1, duration)
                .getResultList();
    }

    @Override
    public List<Album> findAlbumsByDurationCriteria(long duration) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Album> criteriaQuery = cb.createQuery(Album.class);
        Root<Album> album = criteriaQuery.from(Album.class);
        Join<Album, Song> join = album.join("songs");
        criteriaQuery.select(album)
                .groupBy(album)
                .having(cb.lt(cb.sum(join.get("duration")), duration))
                .orderBy(cb.asc(album.get("name")));
        return em.createQuery(criteriaQuery).getResultList();
    }
}
