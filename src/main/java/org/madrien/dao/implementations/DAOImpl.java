package org.madrien.dao.implementations;

import org.madrien.dao.interfaces.DAO;

import javax.persistence.EntityManager;
import javax.persistence.criteria.CriteriaQuery;
import java.util.List;

public abstract class DAOImpl<T> implements DAO<T> {
    protected EntityManager em;
    protected final Class<T> entityClass;

    protected DAOImpl(Class<T> entityClass) {
        this.entityClass = entityClass;
    }

    public void setEntityManager(EntityManager em) {
        this.em = em;
    }

    @Override
    public List<T> findAll() {
        CriteriaQuery<T> c = em.getCriteriaBuilder().createQuery(entityClass);
        c.select(c.from(entityClass));
        return em.createQuery(c).getResultList();
    }

    @Override
    public T findBy(long id) {
        return em.find(entityClass, id);
    }

    @Override
    public void save(T obj) {
        em.persist(obj);
    }

    @Override
    public T update(T obj) {
        return em.merge(obj);
    }

    @Override
    public void delete(long id) {
        em.remove(findBy(id));
    }
}
