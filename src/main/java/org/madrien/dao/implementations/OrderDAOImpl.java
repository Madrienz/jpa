package org.madrien.dao.implementations;

import org.madrien.dao.interfaces.OrderDAO;
import org.madrien.entities.Album;
import org.madrien.entities.Order;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import java.util.List;

public class OrderDAOImpl extends DAOImpl<Order> implements OrderDAO {

    public static final String NATIVE_QUERY = "SELECT * FROM orders "
                                                + "INNER JOIN order_album "
                                                + "ON orders.id = order_album.order_id "
                                                + "INNER JOIN albums "
                                                + "ON albums.id = order_album.album_id "
                                                + "WHERE albums.name = 'Manipulator'; ";

    public static final String JPQL_QUERY = "SELECT o "
                                            + "FROM Order o "
                                            + "JOIN o.albums album "
                                            + "WHERE album.name = :albumName ";

    public OrderDAOImpl() {
        super(Order.class);
    }

    @Override
    public List<Order> findOrdersWithAlbumNameNative() {
        return em.createNativeQuery(NATIVE_QUERY, Order.class).getResultList();
    }

    @Override
    public List<Order> findOrdersWithAlbumNameJPQL(String albumName) {
        return em.createQuery(JPQL_QUERY, Order.class)
                .setParameter("albumName", albumName)
                .getResultList();
    }

    @Override
    public List<Order> findOrdersWithAlbumNameNamed(String albumName) {
        return em.createNamedQuery(Order.FIND_ORDERS_BY_ALBUM_NAME, Order.class)
                .setParameter("albumName", albumName)
                .getResultList();
    }

    @Override
    public List<Order> findOrdersWithAlbumNameCriteria(String albumName) {
        CriteriaBuilder cb = em.getCriteriaBuilder();
        CriteriaQuery<Order> criteriaQuery = cb.createQuery(Order.class);
        Root<Order> order = criteriaQuery.from(Order.class);
        Join<Order, Album> join = order.join("albums");
        criteriaQuery.select(order)
                .where(cb.equal(join.get("name"), albumName));
        return em.createQuery(criteriaQuery).getResultList();
    }
}
