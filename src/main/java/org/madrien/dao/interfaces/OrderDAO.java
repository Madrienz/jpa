package org.madrien.dao.interfaces;

import org.madrien.entities.Order;

import java.util.List;

public interface OrderDAO {
    List<Order> findOrdersWithAlbumNameNative();
    List<Order> findOrdersWithAlbumNameJPQL(String albumName);
    List<Order> findOrdersWithAlbumNameNamed(String albumName);
    List<Order> findOrdersWithAlbumNameCriteria(String albumName);
}
