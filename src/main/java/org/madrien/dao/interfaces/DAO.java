package org.madrien.dao.interfaces;

import java.util.List;

public interface DAO<T> {
    List<T> findAll();
    T findBy(long id);
    void save (T obj);
    T update(T obj);
    void delete(long id);
}
