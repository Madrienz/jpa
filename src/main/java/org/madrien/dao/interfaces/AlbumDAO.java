package org.madrien.dao.interfaces;

import org.madrien.entities.Album;

import java.util.List;

public interface AlbumDAO {
    List<Album> findAlbumsByDurationNative();
    List<Album> findAlbumsByDurationJPQL(long duration);
    List<Album> findAlbumsByDurationNamed(long duration);
    List<Album> findAlbumsByDurationCriteria(long duration);
}
