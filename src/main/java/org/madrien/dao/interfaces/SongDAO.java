package org.madrien.dao.interfaces;

import org.madrien.entities.Song;

import java.util.List;

public interface SongDAO {
    List<Song> findSongsByBandNameNative();
    List<Song> findSongsByBandNameJPQL(String bandName);
    List<Song> findSongsByBandNameNamed(String bandName);
    List<Song> findSongsByBandNameCriteria(String bandName);
}
